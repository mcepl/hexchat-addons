"""
Install as ~/.xchat2/truenames.pl and xchat will auto-load this plugin.

To configure the names to display, edit ~/.xchat2/truenames.conf
each line has the format: nick True Name

Or, use /truename nick True Name to add a true name from inside xchat.
"""
from __future__ import print_function, unicode_literals

import io
import locale
import os.path
import re

import hexchat

__module_name__ = "truenames"
__module_version__ = "1.0"
__module_description__ = "display a configured name in addition to " + \
    "a user's IRC nick, to keep real-life and IRC nicks straight"
# Name:        truenames.pl
# Version:    1.0
# Author:    Joey Hess <id@joeyh.name>
# Date:        2014-01-21
# Description:    display a configured name in addition to a user's IRC nick,
#               to keep real-life and IRC nicks straight
# License:    same as xchat
#
# “Mr. Pollack, have you ever heard of the Mailman?” -- Vinge, True Names
#
# DOCS
# https://hexchat.readthedocs.org/en/latest/script_perl.html
# https://hexchat.readthedocs.org/en/latest/script_python.html


hexchat.prnt("Loading True Names %s" % __module_version__)

conf_file = os.path.join(hexchat.get_info('configdir'), 'truenames.conf')

locale.setlocale(locale.LC_ALL, '')
ENC = locale.getpreferredencoding()


def loadconf():
    out = {}
    with io.open(conf_file, 'r', encoding=ENC) as conf:
        for line in conf:
            line = line.strip()
            nick, name = line.split('', 2)
            out[nick] = name
    return out


def writeconf():
    with io.open(conf_file, "w", encoding=ENC) as conf:
        for nick in truenames.keys():
            print("%s %s" % (nick, truenames[nick]), file=conf)


truenames = loadconf()


def truename(cmd, nick=None, ns=None):
    if ns is not None and ns[0] == "is":
        ns = ns[1:]
    name = ' '.join(ns)

    if nick is None:
        hexchat.prnt("Usage: /truename nick [True Name]")
    elif not ns:
        out = "unknown"
        if truenames[nick] is not None:
            out = truenames[nick]
        hexchat.prnt("%s is %s" % (nick, out))
    else:
        truenames[nick] = name
        writeconf()
        hexchat.prnt("%s is %s" % (nick, name))

    return hexchat.EAT_HEXCHAT


hexchat.hook_command("truename", truename, help="truename nick [True Name]")


def onetrue(msgdata, evt):
    # strip codes on nick, due to colored nicks
    nick = hexchat.strip(msgdata[0])

    if truenames[nick] is not None:
        s = format_event(evt, msgdata)

        # This is a hack; assumes that chat is configured to
        # display <nick> and changes to <True Name|nick>
        # This does not work for eg, Channel Actions,
        # but it's probably ok to omit the true name for those
        # anyway.
        # $s =~ s/</<$truenames{$nick}|/
        s = re.sub('<', '<%s|' % truenames[nick], s)

        hexchat.prnt(s)

        return hexchat.EAT_ALL
    else:
        return hexchat.EAT_NONE


for event in ("Channel Message", "Channel Msg Hilight"):
    hexchat.hook_print(event, onetrue, {'data': event})


## Utiity functions

# Author:       LifeIsPain < idontlikespam (at) orvp [dot] net >
# License:      zlib (attribute me, free to use for whatever)

# Convert Text Events with data into just a string which is returned
#   usage: format_event("Channel Message", @arrayofargs)
def format_event(event, items):
    # force there to be 4 items
    # $#items = 4
    if len(items) < 4:
        for _ in range(len(items), 4):
            items.append('')
    items = items[:4]

    ev_str = hexchat.get_info('event_text %s' % event)

    ev_str = macro_fill(ev_str)

    ev_str = re.sub(r'\$1', items[0], ev_str)
    ev_str = re.sub(r'\$3', items[2], ev_str)
    ev_str = re.sub(r'\$4', items[3], ev_str)
    # $2 may actually have $1, $3, or $4 in it, so do it last
    ev_str = re.sub(r'\$2', items[1], ev_str)

    return ev_str


def macro_short_sub(inf):
    inf_transl = {
        'U': r"\c_", 'B': r"\cB", 'C': r"\cC", 'O': r"\cO",
        'R': r"\cV", 'H': r"\cH", '%': '%'
    }

    if inf in inf_transl:
        return inf_transl[inf]
    return '%' + inf


def macro_fill(arg):
    arg = re.sub(r'%(.)', 'macro_short_sub $1', arg)
    #FIXME arg =~ s/%(.)/macro_short_sub $1/eg
    arg = re.sub(r'\$t', '\t', arg)
    return arg
