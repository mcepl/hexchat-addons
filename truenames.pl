# Name:		truenames.pl
# Version:	1.0
# Author:	Joey Hess <id@joeyh.name>
# Date:		2014-01-21
# Description:	display a configured name in addition to a user's IRC nick,
#               to keep real-life and IRC nicks straight
# License:	same as xchat
#
# “Mr. Pollack, have you ever heard of the Mailman?” -- Vinge, True Names

use strict;
use warnings;
use Xchat qw(:all);

my $version = '1.0';
my $name = 'True Names';

=begin documentation
Install as ~/.xchat2/truenames.pl and xchat will auto-load this plugin.

To configure the names to display, edit ~/.xchat2/truenames.conf
each line has the format: nick True Name

Or, use /truename nick True Name to add a true name from inside xchat.
=end documentation
=cut

prnt("Loading $name $version");

my $conf_file = File::Spec->catfile(get_info('xchatdir'), 'truenames.conf');
my %truenames;

sub loadconf {
	%truenames=();
	if (open(my $conf, "<", $conf_file)) {
		while (<$conf>) {
			chomp;
			my ($nick, $name) = split(" ", $_, 2);
			$truenames{$nick} = $name;
		}
		close $conf;
	}
}

sub writeconf {
	open(my $conf, ">", $conf_file);
	foreach my $nick (keys %truenames) {
		print $conf "$nick $truenames{$nick}\n";
	}
	close $conf;
}

loadconf();

hook_command("truename", \&truename, { help_text => "truename nick [True Name]" });

sub truename {
	my ($cmd, $nick, @ns) = @{$_[0]};
	if (@ns && $ns[0] eq "is") {
		shift @ns;
	}
	my $name=join(" ", @ns);

	if (! defined $nick) {
		prnt("Usage: /truename nick [True Name]");
	}
	elsif (! @ns) {
		prnt("$nick is " . (defined $truenames{$nick} ? $truenames{$nick} : "unknown"));
	}
	else {
		$truenames{$nick}=$name;
		writeconf();
		prnt("$nick is $name");
	}

	return EAT_XCHAT;
}

foreach my $event ("Channel Message", "Channel Msg Hilight") {
	hook_print($event, \&onetrue, { data => $event });
}

sub onetrue {
	my @msgdata = @{shift()};
	my $event = shift;

	# strip codes on nick, due to colored nicks
	my $nick = strip_code($msgdata[0]);

	if (exists $truenames{$nick}) {
		my $s = format_event($event, @msgdata);

		# This is a hack; assumes that chat is configured to
		# display <nick> and changes to <True Name|nick>
		# This does not work for eg, Channel Actions,
		# but it's probably ok to omit the true name for those
		# anyway.
		$s =~ s/</<$truenames{$nick}|/;

		prnt($s);
		
		return EAT_ALL;
	}
	else {
		return EAT_NONE;
	}
}

## Utiity functions

# Author:       LifeIsPain < idontlikespam (at) orvp [dot] net >
# License:      zlib (attribute me, free to use for whatever)

# Convert Text Events with data into just a string which is returned
#   usage: format_event("Channel Message", @arrayofargs)
sub format_event {
	my ($event, @items) = @_;

	# force there to be 4 items
	$#items = 4;
	foreach (@items) {
		$_ = '' unless $_;
	}

	my $string = get_info('event_text ' . $event);

	$string = macro_fill($string);

	$string =~ s/\$1/$items[0]/g;
	$string =~ s/\$3/$items[2]/g;
	$string =~ s/\$4/$items[3]/g;
	$string =~ s/\$2/$items[1]/g; # $2 may actually have $1, $3, or $4 in it, so do it last

	return $string;
}

sub macro_short_sub ($) {
	if    ($_[0] eq 'U') { return "\c_"; }
	elsif ($_[0] eq 'B') { return "\cB"; }
	elsif ($_[0] eq 'C') { return "\cC"; }
	elsif ($_[0] eq 'O') { return "\cO"; }
	elsif ($_[0] eq 'R') { return "\cV"; }
	elsif ($_[0] eq 'H') { return "\cH"; }
	elsif ($_[0] eq '%') { return '%';   }
	return '%'.$_[0];
}

sub macro_fill {
	$_[0] =~ s/%(.)/macro_short_sub $1/eg;
	$_[0] =~ s/\$t/\t/;
	return ($_[0]);
}

__END__
